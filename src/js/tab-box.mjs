const style =
`
  <style>
    :host {
      --theme: #06a7a9;
    }

    ::slotted(*) {
      background-color: white;
      padding: 1rem;
    }

    button {
      border: none;
      background-color: silver;
      padding: 1rem;
    }

    button:not([active]) + button:not([active]) {
      box-shadow: inset 1px 0 0 0 lightgrey;
    }

    button[active] {
      background-color: white;
      box-shadow: inset 0 2px 0 0 var(--theme),
      0 -2px 0 0 var(--theme);
    }

    @media only screen and ( max-width: 32rem) {
      button {
        width: 100%;
      }

      button[active] {
        box-shadow: inset 0 0 0 2px var(--theme);
      }
    }
  </style>
`

const tabButtonsTemplate = titles => titles
  .map
    ( title =>
        `
          <button type=button>
            ${ title }
          </button>
        `
        .trim()
    )
  .join
    ('')

customElements.define('tab-box', class extends HTMLElement {
  constructor() {
    super ()

    this.attachShadow({ mode: 'open' })

    this.init()
  }

  init () {
    const titles = Array.from( this.children )
      .map
        ( child => child.dataset[ this.dataset.attr || 'title' ] )

    const tabs = tabButtonsTemplate( titles )

    this.shadowRoot.innerHTML =
      `
        ${ style }

        ${ tabs }

        <slot>
        </slot>
      `

    Array.from( this.shadowRoot.querySelectorAll('button') )
      .forEach
        ( (	btn, idx	) => btn
            .addEventListener
              ( 'click', evt =>
                  this.setActive( idx )
              )
        )
    
    this.setActive()
  }

  setActive ( index = 0 ) {
    this.buttonActive( index )

    this.tabSwitch( index )
  }

  buttonActive ( index ) {
    Array.from( this.shadowRoot.querySelectorAll( 'button' ) )
      .forEach
        ( ( btn, idx ) =>
            {
              if ( idx === index )
                btn.setAttribute('active', true)
              else
                btn.removeAttribute( 'active' )
            }
        )
  }

  tabSwitch ( index ) {
    Array.from( this.children )
      .forEach
        ( ( child, idx ) =>
            {
              if ( idx === index )
                child.hidden = false
              else
                child.hidden = true
            }
        )
  }
})
