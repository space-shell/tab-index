"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _wrapNativeSuper(Class) { var _cache = typeof Map === "function" ? new Map() : undefined; _wrapNativeSuper = function _wrapNativeSuper(Class) { if (Class === null || !_isNativeFunction(Class)) return Class; if (typeof Class !== "function") { throw new TypeError("Super expression must either be null or a function"); } if (typeof _cache !== "undefined") { if (_cache.has(Class)) return _cache.get(Class); _cache.set(Class, Wrapper); } function Wrapper() { return _construct(Class, arguments, _getPrototypeOf(this).constructor); } Wrapper.prototype = Object.create(Class.prototype, { constructor: { value: Wrapper, enumerable: false, writable: true, configurable: true } }); return _setPrototypeOf(Wrapper, Class); }; return _wrapNativeSuper(Class); }

function isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _construct(Parent, args, Class) { if (isNativeReflectConstruct()) { _construct = Reflect.construct; } else { _construct = function _construct(Parent, args, Class) { var a = [null]; a.push.apply(a, args); var Constructor = Function.bind.apply(Parent, a); var instance = new Constructor(); if (Class) _setPrototypeOf(instance, Class.prototype); return instance; }; } return _construct.apply(null, arguments); }

function _isNativeFunction(fn) { return Function.toString.call(fn).indexOf("[native code]") !== -1; }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var style = "\n  <style>\n    :host {\n      --theme: #06a7a9;\n    }\n\n    ::slotted(*) {\n      background-color: white;\n      padding: 1rem;\n    }\n\n    button {\n      border: none;\n      background-color: silver;\n      padding: 1rem;\n    }\n\n    button:not([active]) + button:not([active]) {\n      box-shadow: inset 1px 0 0 0 lightgrey;\n    }\n\n    button[active] {\n      background-color: white;\n      box-shadow: inset 0 2px 0 0 var(--theme),\n      0 -2px 0 0 var(--theme);\n    }\n\n    @media only screen and ( max-width: 32rem) {\n      button {\n        width: 100%;\n      }\n\n      button[active] {\n        box-shadow: inset 0 0 0 2px var(--theme);\n      }\n    }\n  </style>\n";

var tabButtonsTemplate = function tabButtonsTemplate(titles) {
  return titles.map(function (title) {
    return "\n          <button type=button>\n            ".concat(title, "\n          </button>\n        ").trim();
  }).join('');
};

customElements.define('tab-box',
/*#__PURE__*/
function (_HTMLElement) {
  _inherits(_class, _HTMLElement);

  function _class() {
    var _this;

    _classCallCheck(this, _class);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(_class).call(this));

    _this.attachShadow({
      mode: 'open'
    });

    _this.init();

    return _this;
  }

  _createClass(_class, [{
    key: "init",
    value: function init() {
      var _this2 = this;

      var titles = Array.from(this.children).map(function (child) {
        return child.dataset[_this2.dataset.attr || 'title'];
      });
      var tabs = tabButtonsTemplate(titles);
      this.shadowRoot.innerHTML = "\n        ".concat(style, "\n\n        ").concat(tabs, "\n\n        <slot>\n        </slot>\n      ");
      Array.from(this.shadowRoot.querySelectorAll('button')).forEach(function (btn, idx) {
        return btn.addEventListener('click', function (evt) {
          return _this2.setActive(idx);
        });
      });
      this.setActive();
    }
  }, {
    key: "setActive",
    value: function setActive() {
      var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      this.buttonActive(index);
      this.tabSwitch(index);
    }
  }, {
    key: "buttonActive",
    value: function buttonActive(index) {
      Array.from(this.shadowRoot.querySelectorAll('button')).forEach(function (btn, idx) {
        if (idx === index) btn.setAttribute('active', true);else btn.removeAttribute('active');
      });
    }
  }, {
    key: "tabSwitch",
    value: function tabSwitch(index) {
      Array.from(this.children).forEach(function (child, idx) {
        if (idx === index) child.hidden = false;else child.hidden = true;
      });
    }
  }]);

  return _class;
}(_wrapNativeSuper(HTMLElement)));
